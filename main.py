from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    city_title = db.Column(db.String(100), nullable=False, unique=True)
    streets = db.relationship('Street', backref='city', lazy=False)


class Street(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    street_title = db.Column(db.String(250), nullable=False)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=False)


db.drop_all()
db.create_all()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/cities', methods=["POST", "GET"])
def cities():
    items = City.query.order_by(City.id).all()

    if request.method == "POST":
        city = request.form['city_title']

        item = City(city_title=city)
        item_del = City.query.filter_by(city_title=city).first()
        if request.form['submit'] == "Add":
            try:
                db.session.add(item)
                db.session.commit()
                return redirect('/cities')
            except:
                return "Error occured!"
        if request.form['submit'] == "Delete":
            try:
                db.session.delete(item_del)
                db.session.commit()
                return redirect('/cities')
            except:
                return "Error occured!"
    else:
        return render_template('cities.html', data=items)


@app.route('/cities/<int:city_id>/streets', methods=["POST", "GET"])
def streets(city_id):
    city = City.query.get_or_404(city_id)
    items = Street.query.order_by(Street.street_title).all()
    linked_city = city.city_title

    if request.method == "POST":
        street = request.form['street_title']

        item = Street(street_title=street, city_id=city_id)
        item_del = Street.query.filter_by(street_title=street).first()
        if request.form['submit'] == "Add":
            try:
                db.session.add(item)
                db.session.commit()
                return redirect(request.url)
            except:
                return "Error occured!"
        elif request.form['submit'] == "Delete":
            try:
                db.session.delete(item_del)
                db.session.commit()
                return redirect(request.url)
            except:
                return "Error occured!"
    else:
        return render_template('streets.html', data=items, linked_city=linked_city)


@app.route('/drop', methods=['POST', 'GET'])
def drop():
    if request.method == "POST":
        try:
            db.drop_all()
            db.create_all()
            return redirect('/cities')
        except:
            return "Error of dropping occurred"
    else:
        return render_template('drop.html')


if __name__ == '__main__':
    # app.run(debug=True)
    app.run(host='0.0.0.0', debug=True)
