import requests
from flask import request

cities = ['Nizhniy Novgorod', 'City 17', 'Night City', 'Default City', 'Lazy Town', 'Capital City', 'Big Beevers', 'London',
          'Rome', 'Tristram Village', 'Aden Town', 'Gludio Village', 'Orgrimmar', 'Dalaran', 'Citadel', 'Gotham City',
          'Zion', 'Hogsmeade', 'Meridian', 'Vice City', 'Los Santos', 'El Dorado', 'Agrabah']

streets = ['Bolshaya Pokrovskaya', 'Privet Street', 'Workshop Street', 'Big Business Street', 'Waffle Street',
           'Super Street', 'Meme Street', 'Sesame Street', 'Sandy Street', 'Windy Street', 'Icy Street',
           'Japantown', 'Chinatown', 'Central Street', 'Arbat']

city_amount = 15

if city_amount > len(cities):
    city_amount = len(cities)

for i in range(city_amount):
    if i < len(cities):
        requests.post('http://127.0.0.1:5000/cities', data={'submit': 'Add',
                                                                       'id': 'city_title',
                                                                       'value': "Add",
                                                                       'city_title': str(cities[i])})
    else:
        string = 'Random City Nubmer ' + str(i + 1)
        requests.post('http://127.0.0.1:5000/cities', data={'submit': 'Add',
                                                                       'id': 'city_title',
                                                                       'value': "Add",
                                                                       'city_title': str(string)})

# response = requests.post('http://127.0.0.1:5000/cities', data={'submit': 'Add',
#                                                                'id': 'city_title',
#                                                                'value': "Add",
#                                                                'city_title': 'The City2'})

# street_res = requests.post('http://127.0.0.1:5000/cities/1/streets', data={'submit': 'Add',
#                                                                'id': 'street_title',
#                                                                'value': "Add",
#                                                                'street_title': 'The Street2'})

# print('response from server:\n', response.text)
# print('response from server:\n', street_res.text)

# x = 50
#
# for i in range(x):
#     if i < len(cities):
#         print(cities[i])
#     else:
#         print("Random City Number " + str(i+1))